<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ExpenseType
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 *
 * @property Expense $expenses
 */
class ExpenseType extends Model
{
    protected $table = 'expense_types';

    protected $fillable = [
        'name'
    ];
}
