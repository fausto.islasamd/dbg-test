<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ExpenseCategory
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property Expense $expenses
 */
class ExpenseCategory extends Model
{
    protected $table = 'expense_categories';

    protected $fillable = [
        'name'
    ];

    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }
}
