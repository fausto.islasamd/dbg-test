<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Expense
 * @package App\Models
 *
 * @property int $id
 * @property int $amount
 * @property int $tax_percentage
 *
 * @property Employee $employee
 * @property ExpenseCategory $expense_category
 * @property ExpenseType $expense_type
 */
class Expense extends Model
{
    use SoftDeletes;


    protected $table = 'expenses';

    protected $fillable = [
        'amount',
        'tax_percentage',
        'category_id',
        'employee_id',
    ];

    protected $appends = ['TotalC', 'TaxC'];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function expense_category()
    {
        return $this->belongsTo(ExpenseCategory::class);
    }

    public function payment_method()
    {
        return $this->belongsTo(ExpensePaymentMethod::class, 'expense_payment_method_id');
    }

    public function getTotalCAttribute()
    {
        return ($this->amount * (($this->tax_percentage / 100) + 1)) / 100;
    }

    public function getTaxCAttribute()
    {
        return ($this->amount * ($this->tax_percentage / 100)) / 100;
    }
}
