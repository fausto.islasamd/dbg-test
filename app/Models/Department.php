<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Department
 *
 * @property int $id
 * @property string $name
 */
class Department extends Model
{
    use SoftDeletes;

    protected $appends = ['TotalExpense'];

    protected $table = 'departments';

    protected $fillable = [
        'name'
    ];

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    public function expenses()
    {
        return $this->hasManyThrough(Expense::class, Employee::class);
    }

    public function getTotalExpenseAttribute()
    {
        return $this->expenses()->sum('amount');
    }
}
