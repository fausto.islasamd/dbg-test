<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Employee
 *
 * @property int $id
 * @property int $name
 * @property int $last_name
 * @property int $department_id
 *
 * @property Department $department
 */
class Employee extends Model
{
    protected $table = 'employees';

    protected $fillable = [
        'name',
        'last_name',
        'department_id'
    ];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
