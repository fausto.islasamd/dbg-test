<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpensePaymentMethod extends Model
{
    protected $table = 'expense_payment_methods';
}
