<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Expense;
use App\Models\ExpenseCategory;
use App\Models\ExpensePaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GastosController extends Controller
{
    public function index()
    {
        return view('gastos');
    }

    public function getGastos()
    {
        $exp = Expense::with('employee.department', 'expense_category', 'payment_method')
            ->orderBy('id', 'desc')
            ->get();

        return response()->json([
            'expenses' => $exp
        ]);
    }

    public function getEmployees()
    {
        $emp = Employee::get();
        return response()->json([
            'employees' => $emp
        ]);
    }

    public function getExpenseCategory()
    {
        $category = ExpenseCategory::get();
        return response()->json([
            'expCategory' => $category
        ]);
    }

    public function getPaymentMethod()
    {
        $paymentMethods = ExpensePaymentMethod::get();
        return response()->json([
            'paymentMethods' => $paymentMethods
        ]);
    }

    public function newExpense(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee' => 'required',
            'amount' => 'required|numeric',
            'percentage' => 'required|numeric',
            'category' => 'required',
            'paymentMethod' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'msg' => 'Algunos campos contienen errores',
                'errors' => $validator->getMessageBag()->toArray()
            ]);
        }

        $exp = new Expense();
        $exp->amount = $request->amount * 100;
        $exp->tax_percentage = $request->percentage;
        $exp->employee_id = $request->employee;
        $exp->expense_category_id = $request->category;
        $exp->expense_payment_method_id = $request->paymentMethod;
        $exp->save();


        $exp->load('employee.department', 'expense_category', 'payment_method');

        return response()->json([
            'expense' => $exp
        ]);
    }

    public function editExpense(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee' => 'required',
            'amount' => 'required|numeric',
            'percentage' => 'required|numeric',
            'category' => 'required',
            'paymentMethod' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'msg' => 'Algunos campos contienen errores',
                'errors' => $validator->getMessageBag()->toArray()
            ]);
        }

        $exp = Expense::find($request->expense_id);
        $exp->amount = $request->amount * 100;
        $exp->tax_percentage = $request->percentage;
        $exp->employee_id = $request->employee;
        $exp->expense_category_id = $request->category;
        $exp->expense_payment_method_id = $request->paymentMethod;
        $exp->save();


        $exp->load('employee.department', 'expense_category', 'payment_method');

        return response()->json([
            'expense' => $exp
        ]);
    }

    public function deleteExpense(Request $request)
    {
        $exp = Expense::find($request->expense_id);
        $exp->delete();

        return response()->json([
            'msg' => 'Gasto eliminado correctamente.'
        ]);
    }

}
