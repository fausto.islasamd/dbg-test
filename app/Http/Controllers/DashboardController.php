<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard');
    }

    public function getExpenseByDepartment()
    {
        $exp = Department::with('expenses')->get();
        return response()->json([
            'expenses' => $exp
        ]);
    }
}
