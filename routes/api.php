<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('getExpenseByDepartment','DashboardController@getExpenseByDepartment');
Route::get('getGastos','GastosController@getGastos');
Route::get('getEmployees','GastosController@getEmployees');
Route::get('getExpenseCategory','GastosController@getExpenseCategory');
Route::get('getPaymentMethod','GastosController@getPaymentMethod');
Route::post('newExpense','GastosController@newExpense');
Route::post('editExpense','GastosController@editExpense');
Route::post('deleteExpense','GastosController@deleteExpense');
