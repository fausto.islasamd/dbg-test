require('./bootstrap');

window.Vue = require('vue');

const moment = require('moment')
require('moment/locale/es')

Vue.use(require('vue-moment'), {
    moment
})

Vue.filter('toCurrency', function (value) {
    if (typeof value !== "number") {
        return value;
    }
    let formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 2
    });
    return formatter.format(value);
});

Vue.component('graficas-component', require('./components/Graficas.vue').default);
Vue.component('gastos-component', require('./components/Gastos.vue').default);

const app = new Vue({
    el: "#app"
})
