<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('amount');
            $table->unsignedSmallInteger('tax_percentage')->default(0);
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('expense_category_id');
            $table->unsignedBigInteger('expense_payment_method_id');
            $table->timestamps();


            $table->foreign('employee_id')
                ->references('id')->on('employees')
                ->onDelete('cascade');

            $table->foreign('expense_category_id')
                ->references('id')->on('expense_categories')
                ->onDelete('cascade');

            $table->foreign('expense_payment_method_id')
                ->references('id')->on('expense_payment_methods')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
