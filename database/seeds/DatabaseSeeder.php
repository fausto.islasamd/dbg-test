<?php

use App\Models\Department;
use App\Models\Employee;
use App\Models\ExpenseCategory;
use App\Models\ExpenseType;
use Illuminate\Database\Seeder;

class
DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $departments = [
            "Mantenimiento",
            "Ventas",
            "Calidad",
            "Recursos Humanos",
            "Mejora Continua",
            "Tecnologías de la información",
            "Producción",
            "Almacén",
            "Compras",
            "Finanzas",
            "Mercadotecnia",
            "Legal",
        ];

        $categories = [
            "Telefonía celular",
            "Sueldos",
            "Pachangas",
            "Material de oficina",
            "Insumos",
            "Servicios",
            "Publicidad",
            "Viajes/Transporte",
        ];

        $employees = [
            ['name' => 'Iván', 'last_name' => 'Ortega', 'department' => 'Mantenimiento'],
            ['name' => 'Guillermo', 'last_name' => 'Rodríguez', 'department' => 'Ventas'],
            ['name' => 'Julian', 'last_name' => 'Brito', 'department' => 'Calidad'],
            ['name' => 'Juan', 'last_name' => 'Mendiola', 'department' => 'Recursos Humanos'],
            ['name' => 'Alejandro', 'last_name' => 'Villarreal', 'department' => 'Mejora Continua'],
            ['name' => 'Roberto', 'last_name' => 'Sánchez', 'department' => 'Tecnologías de la información'],
            ['name' => 'Gilberto', 'last_name' => 'Suárez', 'department' => 'Producción'],
            ['name' => 'Jesús', 'last_name' => 'Cortez', 'department' => 'Almacén'],
            ['name' => 'José', 'last_name' => 'Pérez', 'department' => 'Compras'],
            ['name' => 'Vicente', 'last_name' => 'Hérnandez', 'department' => 'Finanzas'],
            ['name' => 'Karla', 'last_name' => 'Guerra', 'department' => 'Mercadotecnia'],
            ['name' => 'Jimena', 'last_name' => 'Reyes', 'department' => 'Legal'],
        ];


        $payment_methods = [
            'Efectivo',
            'Transferencia'
        ];

        $expenses = [
            ['name' => 'Iván', 'last_name' => 'Ortega', 'type' => 'Telefonía celular', 'amount' => '150', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Guillermo', 'last_name' => 'Rodríguez', 'type' => 'Sueldos', 'amount' => '2000', 'payment_methods' => 'Transferencia', 'percentage' => 0],
            ['name' => 'Julian', 'last_name' => 'Brito', 'type' => 'Pachangas', 'amount' => '586.5', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Juan', 'last_name' => 'Mendiola', 'type' => 'Material de oficina', 'amount' => '477.5', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Alejandro', 'last_name' => 'Villarreal', 'type' => 'Pachangas', 'amount' => '71.5', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Roberto', 'last_name' => 'Sánchez', 'type' => 'Insumos', 'amount' => '216.5', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Gilberto', 'last_name' => 'Suárez', 'type' => 'Pachangas', 'amount' => '29.5', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Jesús', 'last_name' => 'Cortez', 'type' => 'Pachangas', 'amount' => '114.5', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Jesús', 'last_name' => 'Cortez', 'type' => 'Telefonía celular', 'amount' => '175', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Jesús', 'last_name' => 'Cortez', 'type' => 'Sueldos', 'amount' => '2000', 'payment_methods' => 'Transferencia', 'percentage' => 0],
            ['name' => 'Alejandro', 'last_name' => 'Villarreal', 'type' => 'Servicios', 'amount' => '885.5', 'payment_methods' => 'Transferencia', 'percentage' => 0.16],
            ['name' => 'José', 'last_name' => 'Pérez', 'type' => 'Telefonía celular', 'amount' => '150', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Guillermo', 'last_name' => 'Rodríguez', 'type' => 'Servicios', 'amount' => '91.5', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Roberto', 'last_name' => 'Sánchez', 'type' => 'Telefonía celular', 'amount' => '150', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Jesús', 'last_name' => 'Cortez', 'type' => 'Sueldos', 'amount' => '2000', 'payment_methods' => 'Transferencia', 'percentage' => 0],
            ['name' => 'José', 'last_name' => 'Pérez', 'type' => 'Material de oficina', 'amount' => '241.5', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Roberto', 'last_name' => 'Sánchez', 'type' => 'Insumos', 'amount' => '12.5', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Roberto', 'last_name' => 'Sánchez', 'type' => 'Servicios', 'amount' => '1336', 'payment_methods' => 'Transferencia', 'percentage' => 0.16],
            ['name' => 'Roberto', 'last_name' => 'Sánchez', 'type' => 'Servicios', 'amount' => '799.5', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Julian', 'last_name' => 'Brito', 'type' => 'Pachangas', 'amount' => '43.5', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Vicente', 'last_name' => 'Hérnandez', 'type' => 'Material de oficina', 'amount' => '60', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Iván', 'last_name' => 'Ortega', 'type' => 'Telefonía celular', 'amount' => '150', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Karla', 'last_name' => 'Guerra', 'type' => 'Publicidad', 'amount' => '298', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Vicente', 'last_name' => 'Hérnandez', 'type' => 'Sueldos', 'amount' => '2000', 'payment_methods' => 'Transferencia', 'percentage' => 0],
            ['name' => 'Alejandro', 'last_name' => 'Villarreal', 'type' => 'Material de oficina', 'amount' => '277', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'José', 'last_name' => 'Pérez', 'type' => 'Telefonía celular', 'amount' => '150', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Vicente', 'last_name' => 'Hérnandez', 'type' => 'Pachangas', 'amount' => '100', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'José', 'last_name' => 'Pérez', 'type' => 'Viajes/Transporte', 'amount' => '188', 'payment_methods' => 'Efectivo', 'percentage' => 0.24],
            ['name' => 'Karla', 'last_name' => 'Guerra', 'type' => 'Sueldos', 'amount' => '2000', 'payment_methods' => 'Transferencia', 'percentage' => 0],
            ['name' => 'Julian', 'last_name' => 'Brito', 'type' => 'Material de oficina', 'amount' => '58', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Guillermo', 'last_name' => 'Rodríguez', 'type' => 'Servicios', 'amount' => '650', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Guillermo', 'last_name' => 'Rodríguez', 'type' => 'Viajes/Transporte', 'amount' => '300', 'payment_methods' => 'Efectivo', 'percentage' => 0.24],
            ['name' => 'José', 'last_name' => 'Pérez', 'type' => 'Telefonía celular', 'amount' => '155', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Jimena', 'last_name' => 'Reyes', 'type' => 'Sueldos', 'amount' => '2000', 'payment_methods' => 'Transferencia', 'percentage' => 0],
            ['name' => 'Alejandro', 'last_name' => 'Villarreal', 'type' => 'Servicios', 'amount' => '205', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Iván', 'last_name' => 'Ortega', 'type' => 'Publicidad', 'amount' => '1200', 'payment_methods' => 'Transferencia', 'percentage' => 0.16],
            ['name' => 'Alejandro', 'last_name' => 'Villarreal', 'type' => 'Telefonía celular', 'amount' => '162', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Julian', 'last_name' => 'Brito', 'type' => 'Sueldos', 'amount' => '2000', 'payment_methods' => 'Transferencia', 'percentage' => 0],
            ['name' => 'Jesús', 'last_name' => 'Cortez', 'type' => 'Servicios', 'amount' => '450', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Roberto', 'last_name' => 'Sánchez', 'type' => 'Insumos', 'amount' => '1150', 'payment_methods' => 'Transferencia', 'percentage' => 0.16],
            ['name' => 'Roberto', 'last_name' => 'Sánchez', 'type' => 'Pachangas', 'amount' => '78', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Jesús', 'last_name' => 'Cortez', 'type' => 'Viajes/Transporte', 'amount' => '235', 'payment_methods' => 'Efectivo', 'percentage' => 0.24],
            ['name' => 'Guillermo', 'last_name' => 'Rodríguez', 'type' => 'Insumos', 'amount' => '400', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Karla', 'last_name' => 'Guerra', 'type' => 'Sueldos', 'amount' => '2000', 'payment_methods' => 'Transferencia', 'percentage' => 0],
            ['name' => 'Gilberto', 'last_name' => 'Suárez', 'type' => 'Pachangas', 'amount' => '85', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Roberto', 'last_name' => 'Sánchez', 'type' => 'Telefonía celular', 'amount' => '150', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Juan', 'last_name' => 'Mendiola', 'type' => 'Servicios', 'amount' => '518', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Guillermo', 'last_name' => 'Rodríguez', 'type' => 'Pachangas', 'amount' => '100', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Julian', 'last_name' => 'Brito', 'type' => 'Insumos', 'amount' => '556', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Juan', 'last_name' => 'Mendiola', 'type' => 'Sueldos', 'amount' => '2000', 'payment_methods' => 'Transferencia', 'percentage' => 0],
            ['name' => 'José', 'last_name' => 'Pérez', 'type' => 'Publicidad', 'amount' => '1500', 'payment_methods' => 'Transferencia', 'percentage' => 0.16],
            ['name' => 'Jimena', 'last_name' => 'Reyes', 'type' => 'Telefonía celular', 'amount' => '150', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Juan', 'last_name' => 'Mendiola', 'type' => 'Servicios', 'amount' => '327', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Guillermo', 'last_name' => 'Rodríguez', 'type' => 'Insumos', 'amount' => '88', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
            ['name' => 'Alejandro', 'last_name' => 'Villarreal', 'type' => 'Viajes/Transporte', 'amount' => '850', 'payment_methods' => 'Efectivo', 'percentage' => 0.16],
        ];

        $departmentsCollection = collect();
        foreach ($departments as $department) {
            $d = new Department();
            $d->name = $department;
            $d->save();
            $departmentsCollection->push($d);
        }

        $categoryCollection = collect();
        foreach ($categories as $category) {
            $c = new ExpenseCategory();
            $c->name = $category;
            $c->save();
            $categoryCollection->push($c);
        }

        $paymentMethodsCollection = collect();
        foreach ($payment_methods as $payment_method) {
            $pm = new \App\Models\ExpensePaymentMethod();
            $pm->name = $payment_method;
            $pm->save();
            $paymentMethodsCollection->push($pm);

        }

        $employeesCollection = collect();
        foreach ($employees as $employee) {
            $e = new Employee();
            $e->name = $employee['name'];
            $e->last_name = $employee['last_name'];
            $dId = $departmentsCollection->where('name', '=', $employee['department'])->first();
            $e->department_id = $dId->id;
            $e->save();
            $employeesCollection->push($e);
        }

        foreach ($expenses as $expens) {
            $ex = new \App\Models\Expense();
            $ex->amount = $expens['amount'] * 100; // Stored as cents
            $em = $employeesCollection->where('name', '=', $expens['name'])
                ->where('last_name', '=', $expens['last_name'])->first();
            $ex->employee_id = $em->id;
            $pm = $paymentMethodsCollection->where('name', '=', $expens['payment_methods'])->first();
            $ex->expense_payment_method_id = $pm->id;
            $cat = $categoryCollection->where('name', '=', $expens['type'])->first();
            $ex->expense_category_id = $cat->id;
            $ex->tax_percentage = $expens['percentage'] * 100;
            $ex->save();
        }


    }
}
